"use client"
import { useEffect, useState } from "react"
import "./page.css"
import { deleteUser, getUserDesactiver, modifCytoyent } from "@/lib/features/citoyens/citoyens"
import { useDispatch } from "react-redux"
import Image from "next/image"

export default function AdminPage() {
    const [maxH,setH]=useState("")

    const [listeCyt,setListeCyt]=useState([])
    const [selected,setSelected]=useState()


    const dispatch = useDispatch()

    const [A,setA]=useState(0)
    useEffect(function() {
       const h=Math.round(document.querySelector('.__cont__').getBoundingClientRect().height)+"px"
       setH(h);

        dispatch(getUserDesactiver()).then(function ({payload}) {
            setListeCyt(payload.data)
            setSelected(payload.data[0])
            console.log(payload.data[0]);
        })
    },[A])

    const handleRefuseInscription=function () {
        dispatch(deleteUser(selected.numCIN)).then(function({payload}) {
            setA(()=>(A+1));
        })
    }
    
    const handleAccepteInscription=function() {
        dispatch(modifCytoyent(selected)).then(function({payload}) {
            setA(()=>(A+1));
        })
    }
    
    return <main className="d-flex flex-column align-items-center justify-content-start __admin_page__">
        <div className="d-flex align-items-center justify-content-between flex-grow-1 col-12 border __list_inscription__">
            <div className="d-flex flex-column shadow-lg align-items-center justify-content-start col-6 col-md-3 h-100">
                <div className="col-12 __titre_liste__" style={{height:"60px"}}> Liste des inscription </div>
                <div className="d-flex flex-column align-items-center justify-content-start col-12 flex-grow-1 __cont__" style={{maxHeight:'724px'}}>
                    
                    {
                        listeCyt.map((cyt,index)=>(
                            <div onClick={()=>{setSelected(cyt);console.log(cyt);}} key={index} className="col-11 text-gray mt-2 border shadow rounded-2 __l_insc_item__">
                                <div className="m-1 __img_"></div>
                                <div className="m-1 col "> 
                                    {`${cyt.nom} ${cyt.prenom}`}
                                </div>
                            </div>
                        ))
                    }
                    
                </div>
            </div>

            {
                selected ? 
                <div className="text-light d-flex flex-column align-items-center justify-content-start col h-100">
                    <div className="d-flex align-items-center justify-content-start mt-2 col-11 __about_user__">
                        <div className="border __pdp_user__"> 
                            <img src={selected.photo} alt="img" />
                        </div>

                        <div className="m-5 mt-0 mb-0 h-100 col">
                            <h3 style={{fontWeight:"bold"}}>{`${selected.nom} ${selected.prenom}`}</h3>
                            <h5>Numero CIN: {selected.numCIN}</h5>
                            <h5>Proffession: {selected.proffession} </h5>
                            <h5>Adresse: {selected.adresse} </h5>
                            <h5>Sexe: {selected.sexe} </h5>
                        </div>
                    </div>

                    <div className="d-flex align-items-center justify-content-start mt-3 col-11 __CIN_user__">
                        <div className="col h-100 border d-flex align-items-center justify-content-center __CIN_user_item__" style={{marginRight:"10px"}}>
                            <img src={selected.ciNrecto} alt="img" />
                        </div>
                        <div className="col h-100 border d-flex align-items-center justify-content-center __CIN_user_item__" style={{marginLeft:"10px"}}>
                            <img src={selected.ciNverso} alt="img" />
                        </div>
                    </div>    

                    <div className="d-flex flex-column align-items-center justify-content-end mt-3 col-11 flex-grow-1">
                        <div className="col-12 d-flex mb-3">
                            <button onClick={handleAccepteInscription} className="col btn btn-primary m-2">Accepter inscription</button>
                            <button onClick={handleRefuseInscription} className="col btn btn-danger m-2">Refuser inscription</button>
                        </div>
                    </div>        
                </div>
                :
                <div className="text-light d-flex flex-column align-items-center justify-content-center text-center col h-100">
                    Veuillez selectionner un ele ment sur la liste a coté
                </div>
            }
        </div>
    </main>
}