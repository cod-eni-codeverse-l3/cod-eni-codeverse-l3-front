"use client"
import { useCallback, useContext, useState } from "react"
import './page.css'
import { AvatarIcon, EyeClosedIcon, EyeOpenIcon, InfoCircledIcon } from "@radix-ui/react-icons"
import DropZone from "@/_components/DropZone/DropZone"
import { useRouter } from "next/navigation"
import Modal from "@/_components/Modal/Modal"
import { Circles } from "react-loader-spinner"
import { useDispatch } from "react-redux"
import { login } from "@/lib/features/citoyens/citoyens"
import { senDuser } from "@/lib/services/userService"

export default function LoginPage() {
    const [user,setUser]=useState({})
    const [showLoadign,setShowLoading]=useState(false)
    const [showAlert,setShowAlert]=useState(false)
    const [alertTST,setAlertTXT]=useState("")
    const router = useRouter()
    const dispatch=useDispatch()

    const onSubmmit=function(event) {
        event.preventDefault()
        
        const numCIN=document.getElementById('numCIN').value
        const mdp=document.getElementById('Mdp').value

        setUser({numCIN,mdp})
        setShowLoading(true)
        dispatch(login({numCIN,mdp})).then(function({payload}) {
            if (payload.data != "tsisy_lti_a") {
                senDuser({numCIN,mdp})
                router.push('/cytoyens/'+numCIN)
            }else{
                router.prefetch()
                setShowLoading(false)
                setAlertTXT('Information incorrect')
                setShowAlert(true)
                setTimeout(() => {
                    setShowAlert(false)
                }, 2500);
            }
        })
    }

    return <>
    <Modal className='d-flex align-items-center justify-content-center' show={showLoadign} setShow={setShowLoading} noHeader noFooter>
        <Circles></Circles>
    </Modal>
    <main className={" d-flex align-items-center justify-content-center text-light __login_page__"}>
                     
                <form onSubmit={onSubmmit} className={"d-flex align-items-center flex-column col-9 col-sm-6 col-md-4 col-lg-3 rounded-2 __container__ "} style={{position:"relative"}}>
                {
                        showAlert && <div className="m-2 alert bg-danger text-light __alert__" style={{position:"absolute",top:"0",left:"0"}}>
                        {alertTST}
                    </div>
                    }
                
                <div className="d-flex align-items-center justify-content-center col-12 _person_">
                    <div className="_img_">
                        <AvatarIcon></AvatarIcon>
                    </div>
                </div>
                
                <h3 className="mt-1">Se connecter</h3>
                
                <div className="col-10 mt-2 input-box">
                    <label htmlFor="numCIN" className="form-label">Numero CIN: </label>
                    <input defaultValue={user?.numCIN} type="text" name="numCIN" id="numCIN" className="form-control" required/>
                </div>
                <div className="col-10 mt-2 input-box">
                    <label htmlFor="Mdp" className="form-label">Mot de passe: </label>
                    <input defaultValue={user?.mdp} type="password" name="Mdp" id="Mdp" className="form-control" required/>
                </div>

                <div className="col-10 mt-2 mb-2 d-flex align-items-center justify-content-between">
                    <button onClick={()=>{router.push("/cytoyens")}} type="reset" className="btn btn-danger">Retour</button>
                    <button type="submit" className="btn btn-success">Suivant</button>
                </div>

                <div className="col-10 mt-2 mb-2 d-flex align-items-center justify-content-between">
                    <button onClick={()=>{router.push('/cytoyens/signup')}} type="reset" className="col btn btn-primary">S'inscrire</button>
                </div>
                
            </form>
    </main>
    </>
}