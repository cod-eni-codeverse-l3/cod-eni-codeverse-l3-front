import Link from "next/link";

export default function Page() {
    return <div className="col-12 m-0 h-100 d-flex flex-column align-items-center justify-content-stqrt">
        <h4 className="mt-3 text-center col-11 col-md-6 col-lg-4"> Veuiller attendre la verification et l'activation de votre compte  </h4>
        <Link href="/cytoyens/login"> <button className="btn btn-primary"> reesayer </button> </Link>
    </div>
} 