"use client"
import { useCallback, useContext, useState } from "react"
import './page.css'
import { AvatarIcon, EyeClosedIcon, EyeOpenIcon, InfoCircledIcon } from "@radix-ui/react-icons"
import DropZone from "@/_components/DropZone/DropZone"
import { useRouter } from "next/navigation"
import { useDispatch } from "react-redux"
import { createCytoyent, login, uplodFile, verifierCIN } from "@/lib/features/citoyens/citoyens"
import Modal from "@/_components/Modal/Modal"

import { FidgetSpinner,Circles } from "react-loader-spinner"
import { senDuser } from "@/lib/services/userService"

export default function LoginPage() {
    const [user,setUser]=useState({})
    const [step,setStep]=useState(1)
    const [pdp,setPDP]=useState(null)
    const [CINRect,setCINRect]=useState(null)
    const [CINVerso,setCINVerso]=useState(null)
    const [showLoadign,setShowLoading]=useState(false)
    const router = useRouter()
    const [showAlert,setShowAlert]=useState(false)
    const [alertTST,setAlertTXT]=useState("")

    const onSubmmitFirst=function(event) {
        event.preventDefault()
        
        const numCIN=document.getElementById('numCIN').value
        const nom=document.getElementById('Nom').value
        const prenom=document.getElementById('Prenom').value
        const mdp=document.getElementById('Mdp').value

        setUser(()=>({...user,numCIN,nom,prenom,mdp}));
        setStep(2)
    }

    const onSubmitSecond=function (event) {
        event.preventDefault()
        if (pdp && CINRect && CINVerso) {
            const pdpData=new FormData()
            pdpData.append("file",pdp)
            const CINRectData=new FormData()
            CINRectData.append("file",CINRect)
            const CINVersoData=new FormData()
            CINVersoData.append("file",CINVerso)

            const tmp ={...user,pdp:pdpData,CINRect:CINRectData,CINVerso:CINVersoData,role:"cytoyen",etat:"desactivee"}
            
            setUser(tmp);
            setStep(3)
        }
    }

    const dispach=useDispatch()
    const onSubmitThird=function (event) {
        event.preventDefault()
        const Proffession=document.getElementById('Profession').value
        const dateNaiss=document.getElementById('dateNaiss').value
        const Adresse=document.getElementById('Adresse').value
        const Sexe=document.getElementById('Sexe').value
        setUser(()=>({...user,Proffession,dateNaiss,Adresse,Sexe}));
        setShowLoading(true)
        //
        let _pdp=""
        let _CINRect=""
        let _CINVerso=""

        dispach(verifierCIN(user.numCIN)).then(function({payload}) {
            if (!payload) {
                return  dispach(uplodFile(user.pdp))
            }else throw new Error('Le Numéro CIN existe deja')
        }).then(function({payload}) {
            _pdp=payload.data
            return dispach(uplodFile(user.CINRect))
        }).then(function ({payload}) {
            _CINRect=payload.data
            return dispach(uplodFile(user.CINVerso))
        }).then(function ({payload}) {
            _CINVerso=payload.data
            const tmp={...user,pdp:_pdp,CINRect:_CINRect,CINVerso:_CINVerso,Proffession,dateNaiss,Adresse,Sexe}
            setUser(tmp);
            return dispach(createCytoyent(tmp))
        }).then(function () {
            //login
            senDuser({numCIN:user.numCIN,mdp:user.mdp})
            router.push('/cytoyens/'+user.numCIN)
        })
        
        .catch(function (err) {
            setAlertTXT(err.message)
            setShowAlert(true)
            setShowLoading(false)
            setTimeout(() => {
                setShowAlert(false)
            }, 3000);
            setStep(1)
        })
    }

    
    
    return <>
    <Modal className='d-flex align-items-center justify-content-center' show={showLoadign} setShow={setShowLoading} noHeader noFooter>
        <Circles></Circles>
    </Modal>
    <main className={" d-flex align-items-center justify-content-center text-light __signup_page__"}>
        
        {
            (step==1)?
                <form onSubmit={onSubmmitFirst} style={{position:'relative'}} className={"d-flex align-items-center flex-column col-10 col-sm-7 col-md-5 col-lg-4 rounded-2 __container__ "}>
                    
                    {
                        showAlert && <div className="alert bg-danger text-light __alert__" style={{position:"absolute",top:"0",left:"0"}}>
                        {alertTST}
                    </div>
                    }

                <div className="d-flex align-items-center justify-content-center col-12 _person_">
                    <div className="_img_">
                        <AvatarIcon></AvatarIcon>
                    </div>
                </div>
                
                <h3 className="mt-1">S'inscrire</h3>
                
                <div className="col-10 mt-2 input-box">
                    <label htmlFor="numCIN" className="form-label">Numero CIN: </label>
                    <input defaultValue={user?.numCIN} type="text" name="numCIN" id="numCIN" className="form-control" required/>
                </div>
                <div className="col-10 mt-2 input-box">
                    <label htmlFor="Nom" className="form-label">Nom: </label>
                    <input defaultValue={user?.nom} type="text" name="Nom" id="Nom" className="form-control" required/>
                </div>
                <div className="col-10 mt-2 input-box">
                    <label htmlFor="Prenom" className="form-label">Prenom: </label>
                    <input defaultValue={user?.prenom} type="text" name="Prenom" id="Prenom" className="form-control" required/>
                </div>
                <div className="col-10 mt-2 input-box">
                    <label htmlFor="Mdp" className="form-label">Mot depasse: </label>
                    <input defaultValue={user?.mdp} type="password" name="Mdp" id="Mdp" className="form-control" required/>
                </div>
                

                <div className="col-10 mt-2 mb-2 d-flex align-items-center justify-content-between">
                    <button onClick={()=>{router.push("/cytoyens")}} type="reset" className="btn btn-danger">Annuler</button>
                    <button type="submit" className="btn btn-primary">Suivant</button>
                </div>

                <div className="col-10 mt-2 mb-2 d-flex align-items-center justify-content-between">
                    <button onClick={()=>{router.push("/cytoyens/login")}} type="reset" className="col btn btn-success">Se connecter</button>
                </div>
                
            </form>
            
            :
            
            (step==2)?
                <form onSubmit={onSubmitSecond} className={"d-flex align-items-center flex-column col-10 col-sm-7 col-md-5 col-lg-4 rounded-2 __container__ "}>
                    
                    <div className="col-10 mt-2 input-box">
                        <label htmlFor="dateNaiss" className="form-label">Photos de profil: </label>
                        <DropZone file={pdp} setFile={setPDP} acceptedExt={["png",'jpg',"jpeg"]}></DropZone>
                    </div>
                    <div className="col-10 mt-2 input-box">
                        <label htmlFor="dateNaiss" className="form-label">Photos de CIN Recto: </label>
                        <DropZone file={CINRect} setFile={setCINRect} acceptedExt={["png",'jpg',"jpeg"]}></DropZone>
                    </div>
                    <div className="col-10 mt-2 input-box">
                        <label htmlFor="dateNaiss" className="form-label">Photos de CIN Verso: </label>
                        <DropZone file={CINVerso} setFile={setCINVerso} acceptedExt={["png",'jpg',"jpeg"]}></DropZone>
                    </div>

                    <div className="col-10 mt-2 mb-2 d-flex align-items-center justify-content-between">
                        <button type="reset" onClick={()=>{setStep(1)}} className="btn btn-danger">Precedant</button>
                        <button type="submit" className="btn btn-primary">Suivant</button>
                    </div>
                    
                </form>

                :

                <form onSubmit={onSubmitThird} className={"d-flex align-items-center flex-column col-10 col-sm-7 col-md-5 col-lg-4 rounded-2 __container__ "}>
                
                    <div className="col-10 mt-2 input-box">
                        <label htmlFor="Profession" className="form-label">Profession: </label>
                        <input defaultValue={user?.Proffession} type="text" name="Profession" id="Profession" className="form-control" required/>
                    </div>
                    
                    <div className="col-10 mt-2 input-box">
                        <label htmlFor="Adresse" className="form-label">Adresse: </label>
                        <input defaultValue={user?.Adresse} type="text" name="Adresse" id="Adresse" className="form-control" required/>
                    </div>
                    
                    <div className="col-10 mt-2 input-box">
                        <label htmlFor="dateNaiss" className="form-label">Date de naissance: </label>
                        <input defaultValue={user?.dateNaiss} type="date" name="dateNaiss" id="dateNaiss" className="form-control" required/>
                    </div>

                    <div className="col-10 mt-2 input-box">
                        <label htmlFor="Sexe" className="form-label">Sexe: </label>
                        <select defaultValue={user?.Sexe} className="form-control" name="Sexe" id="Sexe" required>
                            <option className="form-control" value=""></option>
                            <option className="form-control" value="Homme">Homme</option>
                            <option className="form-control" value="Femme">Femme</option>
                            <option className="form-control" value="Autres">Autres</option>
                        </select>
                    </div>

                    <div className="col-10 mt-2 mb-2 d-flex align-items-center justify-content-between">
                        <button type="reset" onClick={()=>{setStep(2)}} className="btn btn-danger">Precedant</button>
                        <button type="submit" className="btn btn-primary">S'inscrire</button>
                    </div>
                    
                </form>
        }
    </main>
    </>
}