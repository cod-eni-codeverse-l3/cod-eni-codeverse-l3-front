"use client"
import { createContext, useContext, useEffect, useState } from "react"
import { userContent } from "../layout"
import { useDispatch } from "react-redux"
import { getListeDemmandeByNumCin } from "@/lib/features/demande/demande"
export const listeDemmandeConstext=createContext()

export default function name({children}) {
    const [listeDemmand,setListeDemand]=useState([])
    const [user,setUser]=useContext(userContent)
    const dispatch=useDispatch()
    const [a,setA]=useState(0)

    const  refresh=function() {
        setA(()=>(a+1))
    }
    useEffect(function () {
        dispatch(getListeDemmandeByNumCin(user.numCIN)).then(function({payload}) {
            if (payload.data != "tsisy") {
                setListeDemand(payload.data)
            }else setListeDemand([])
        })
    },[a])
    return <listeDemmandeConstext.Provider value={[listeDemmand,setListeDemand,refresh]}>
        {children}
    </listeDemmandeConstext.Provider>
}