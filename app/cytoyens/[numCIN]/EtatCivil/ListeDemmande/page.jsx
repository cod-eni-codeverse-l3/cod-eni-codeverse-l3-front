"use client"
import CytNavBar from '@/_components/Cytoyens/CytNavBar'
import './page.css'
import {  PlusCircledIcon } from '@radix-ui/react-icons'
import AboutDemande from '@/_components/Cytoyens/EtatCivile/AboutDemande'
import Modal from '@/_components/Modal/Modal'
import { useEffect, useState,useContext } from 'react'
import { typeDemmand,getDemmendByType, ACT_NAISS, ACT_DECES, LEGA_PCIN, CHANGE_ADRESS } from './typeDemmand'
import DropZone from '@/_components/DropZone/DropZone'
import { userContent } from '../../layout'
import { useDispatch } from 'react-redux'
import { createDemmande } from '@/lib/features/demande/demande'
import { uplodFile } from '@/lib/features/citoyens/citoyens'
import { Circles } from 'react-loader-spinner'
import { listeDemmandeConstext } from '../layout'

export default function ListeDemmande() {
    const [selectedTypeDemmand,setSelectedTypeDemmand]=useState(ACT_NAISS)
    const [dem,setDem]=useState(null)
    const [showAdd,setShowAdd]=useState(false)
    const [fichierRequis,setFichierRequis]=useState(null)
    const [user,setUser]=useContext(userContent)
    const dispatch=useDispatch()
    const [showAlert,setShowAlert]=useState(false)
    const [alertTST,setAlertTXT]=useState("")
    const [showLoadign,setShowLoading]=useState(false)
    const [listeDemmande,setListeDemmande,refreshListe]=useContext(listeDemmandeConstext)
    const [selected,setSelected]=useState(listeDemmande[0])

    const onAddDemmand=function() {
        const serviceDemmande=getDemmendByType(document.getElementById('obj').value).text
        let maDate = new Date(); // Par défaut, c'est la date et l'heure actuelle

        // Extraire les composants de la date
        let annee = maDate.getFullYear();
        let mois = ('0' + (maDate.getMonth() + 1)).slice(-2); // +1 car les mois sont indexés de 0 à 11
        let jour = ('0' + maDate.getDate()).slice(-2);
        const dateDemmande=`${annee}-${mois}-${jour}`;
        const numCINDemmande=user.numCIN
        const idTicket="";
        const numTel=document.getElementById('numtel').value
        const RefMvola=document.getElementById('refMvola').value
        let listeFic='';
        const infoRequis=document.getElementById('infos').value
        if (serviceDemmande.trim()!="" && dateDemmande!=null && numTel.trim()!="" && RefMvola.trim()!="") {
            setShowLoading(true)
            Promise.resolve().then(function() {
                if (dem.besoinFichier && fichierRequis) {
                    const formData=new FormData()
                    formData.append('file',fichierRequis)
                    return dispatch(uplodFile(formData))
                }else{
                    return Promise.resolve("")
                }
            }).then(function({payload}) {
                listeFic=(payload)?payload.data : "";
                return dispatch(createDemmande({serviceDemmande,dateDemmande,numCINDemmande,idTicket,numTel,RefMvola,listeFic,infoRequis}))
            }).then(function({payload}) {
                console.log(payload.data);
                setShowLoading(false)
                setShowAdd(false)
                refreshListe()
            })
        }else{
            setAlertTXT('informations invalide')
            setShowAlert(true)
            setTimeout(() => {
                setShowAlert(false)
            }, 2500);
        }
        
        
    }

    useEffect(function() {
        console.log(getDemmendByType(selectedTypeDemmand));
        setDem(getDemmendByType(selectedTypeDemmand))
    },[selectedTypeDemmand])
    

    return  <>
    <Modal className='d-flex align-items-center justify-content-center' show={showLoadign} setShow={setShowLoading} noHeader noFooter>
        <Circles></Circles>
    </Modal>
        <Modal className="d-flex flex-column align-items-center justify-content-start __modat_content__" show={showAdd} setShow={setShowAdd} noHeader confirmAction={onAddDemmand}>
                    {
                        showAlert && <div className="alert bg-danger text-light __alert__" style={{position:"absolute",top:"0",left:"0"}}>
                        {alertTST}
                    </div>
                    }
            <h3>Demande de document</h3>

            <div className="col-10 mt-3">
                <label htmlFor="obj" className="form-label">Objet demmandée</label>
                <select defaultValue={selectedTypeDemmand} onChange={(event)=>{setSelectedTypeDemmand(event.target.value)}} name="obj" id="obj" className="form-control">
                    <option value={ACT_NAISS}> Acte de Naissance </option>
                    <option value={ACT_DECES}> Acte de deces </option>
                    <option value={LEGA_PCIN}> Legalisation Photocopie CIN  </option>
                    <option value={CHANGE_ADRESS}> Changement d'adresse </option>
                </select>
                { dem?.besoinDescTXT && <div id="demandHelp" className="form-text">Besoin de: {dem.infosRequise} </div> }
            </div>

            {
                dem?.besoinDescTXT &&
                    <div className="col-10 mt-3">
                        <label htmlFor="infos" className="form-label">Informations requises: </label>
                        <textarea name="infos" id="infos" className="form-control" style={{maxHeight:"170px"}}></textarea>
                    </div>
            }

            {
                dem?.besoinFichier &&
                    <div className="col-10 mt-3">
                        <label htmlFor="fichier" className="form-label">Fichier requises: </label>
                        <DropZone textColor="text-dark" viewHeight="60px" file={fichierRequis} setFile={setFichierRequis} acceptedExt={["*"]}></DropZone>
                        <div id="demandHelp" className="form-text">Compresser si plusieurs fichier est requise </div>
                    </div>
            }

                    <div className="col-10 mt-3">
                        <label htmlFor="numtel" className="form-label">Numero de telephone Mvola: </label>
                        <input type="tel" name="numtel" id="numtel" className="form-control" />
                    </div>

                    <div className="col-10 mt-3">
                        <label htmlFor="refMvola" className="form-label">Réference tranfert Mvola: </label>
                        <input type="text" name="refMvola" id="refMvola" className="form-control" />
                    </div>
            
        </Modal>
        <main className="d-flex flex-column align-items-center justify-content-start __admin_page__">
            <CytNavBar routeName="EtatCivil"></CytNavBar>

            <div className="d-flex align-items-center justify-content-between flex-grow-1 col-12 __list_inscription__">
                <div className=" _z_ d-flex flex-column shadow-lg align-items-center justify-content-start col-6 col-md-3 h-100">
                    <div className="col-12 __titre_liste__" style={{height:"60px"}}> Liste des demandes</div>
                    <div className="text-light d-flex flex-column align-items-center justify-content-start col-12 flex-grow-1 __cont__" style={{maxHeight:'calc(100vh - 120px - 60px)'}}>
                        
                        {
                            listeDemmande.map((demande,index)=><div onClick={()=>{setSelected(demande)}} key={index} className="col-11 text-gray mt-2 border shadow rounded-2 __l_insc_item__">
                            <div className="m-1 text-light bg-dark shadow-sm __img_"> {demande.serviceDemmande[0].toUpperCase()} </div>
                            <div className="m-1 col d-flex flex-column" style={{overflow:"hidden"}}> 
                                <span style={{whiteSpace:"nowrap",textOverflow:"elipsis"}}> {demande.serviceDemmande} </span>
                                <span>le {demande.dateDemmande}</span>
                            </div>
                        </div>)
                        }
                        
                    </div>
                    <div onClick={()=>{setShowAdd(true)}} className="col-12 d-flex align-items-center justify-content-between __titre_liste__" style={{height:"60px",cursor:"pointer"}}> Envoyer une demmande <PlusCircledIcon></PlusCircledIcon> </div>
                </div>

                <AboutDemande demande={selected}></AboutDemande>
            </div>
    </main>
    </>
}