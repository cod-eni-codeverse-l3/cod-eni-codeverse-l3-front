export const ACT_NAISS=1;
export const ACT_DECES=2;
export const LEGA_PCIN=3;
export const CHANGE_ADRESS=4;

export const typeDemmand=[
    {
        typeDemmand:ACT_NAISS,
        infosRequise:"Nom, ",
        besoinDescTXT:true,
        besoinFichier:false,
        text:"Acte de Naissance"
    },
    {
        typeDemmand:ACT_DECES,
        infosRequise:"",
        besoinDescTXT:true,
        besoinFichier:true,
        text:"Acte de deces"
    },
    {
        typeDemmand:LEGA_PCIN,
        infosRequise:"",
        besoinDescTXT:true,
        besoinFichier:true,
        text:"Legalisation Photocopie CIN"
    },
    {
        typeDemmand:CHANGE_ADRESS,
        infosRequise:"",
        besoinDescTXT:true,
        besoinFichier:true,
        text:"Changement d'adresse"
    }
]

export function getDemmendByType (type) {
    let tmp={};
    typeDemmand.forEach(element => {
        if (element.typeDemmand==type) {
            tmp={...element}
        }
    });
    return tmp
}