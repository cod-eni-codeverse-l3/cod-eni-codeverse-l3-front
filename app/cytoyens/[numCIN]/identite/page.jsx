"use client"
import CytNavBar from '@/_components/Cytoyens/CytNavBar'
import './page.css'
import {  PlusCircledIcon } from '@radix-ui/react-icons'
import AboutDemande from '@/_components/Cytoyens/EtatCivile/AboutDemande'

export default function ListeDemmande() {

    return  <>
        <main className="d-flex flex-column align-items-center justify-content-start __admin_page___">
            
            <CytNavBar routeName="Identite"></CytNavBar>

            <div className="d-flex align-items-center justify-content-between flex-grow-1 col-12 __list_inscription___">
                <div className="d-flex flex-column shadow-lg align-items-center justify-content-start col-6 h-100">
                    <div className="d-flex flex-column align-items-center justify-content-start m-3 flex-grow-1 bg-light rounded" style={{width:"80%"}}>
                        <div className="mt-3 __identity_img____"></div>

                        <div className="col-9 m-2 mt-3 d-flex align-items-center justify-content-between" style={{fontSize:"17px",fontWeight:"bold"}}> <span className='__ident_infos__'>Numero CIN:</span> <span className='__ident_infos_val__'>000000000</span> </div>
                        <div className="col-9 m-2 mt-3 d-flex align-items-center justify-content-between" style={{fontSize:"17px",fontWeight:"bold"}}> <span className='__ident_infos__'>Nom:</span> <span className='__ident_infos_val__'>ANDROANTSIMAVINA</span> </div>
                        <div className="col-9 m-2 mt-3 d-flex align-items-center justify-content-between" style={{fontSize:"17px",fontWeight:"bold"}}> <span className='__ident_infos__'>Prenom:</span> <span className='__ident_infos_val__'>Tsiferana Heritsilavo</span> </div>
                        <div className="col-9 m-2 mt-3 d-flex align-items-center justify-content-between" style={{fontSize:"17px",fontWeight:"bold"}}> <span className='__ident_infos__'>Date de naissance:</span> <span className='__ident_infos_val__'>00/00/0000</span> </div>
                        <div className="col-9 m-2 mt-3 d-flex align-items-center justify-content-between" style={{fontSize:"17px",fontWeight:"bold"}}> <span className='__ident_infos__'>Sexe:</span> <span className='__ident_infos_val__'>Homme</span> </div>
                        <div className="col-9 m-2 mt-3 d-flex align-items-center justify-content-between" style={{fontSize:"17px",fontWeight:"bold"}}> <span className='__ident_infos__'>Adresse:</span> <span className='__ident_infos_val__'>Andalantery</span> </div>
                        <div className="col-9 m-2 mt-3 d-flex align-items-center justify-content-between" style={{fontSize:"17px",fontWeight:"bold"}}> <span className='__ident_infos__'>Profession:</span> <span className='__ident_infos_val__'>Docteur</span> </div>
                    </div>
                </div>

                <div className="d-flex flex-column shadow-lg align-items-center justify-content-between col-6 h-100">
                    <div className="d-flex flex-column align-items-center justify-content-start m-3  bg-light rounded" style={{width:"70%",height:"45%"}}></div>
                    <div className="d-flex flex-column align-items-center justify-content-start m-3  bg-light rounded" style={{width:"70%",height:"45%"}}></div>
                </div>
            </div>
    </main>
    </>
}