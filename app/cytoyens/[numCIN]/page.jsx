import CytNavBar from "@/_components/Cytoyens/CytNavBar"
import "./page.css"
import AcceuilContent from "@/_components/AcceuilContent/AcceuilContent"

export default function PageNumCIn() {
    return <main className="col-12 h-100 __page_cyt__" style={{scrollBehavior:"smooth"}}>
        <CytNavBar routeName='Accueil'></CytNavBar>
        <AcceuilContent></AcceuilContent>
    </main>
}