"use client"
import { useState,createContext, useEffect,useRef } from "react"
import { Circles } from "react-loader-spinner"
import Modal from "@/_components/Modal/Modal"
import { useDispatch } from "react-redux"
import { isLogged, logOut } from "@/lib/services/userService"
import { useRouter } from "next/navigation"
import { verifierCIN } from "@/lib/features/citoyens/citoyens"

export const userContent=createContext([])
export default function Layout({children,params}) {
    const [user,setUser]=useState({})
    const dispatch=useDispatch()
    const router = useRouter()
    const [showLoadign,setShowLoading]=useState(true)

    const V=useRef(true)
    useEffect(function() {
      if (V.current) {
        
        dispatch(verifierCIN(params.numCIN)).then(function({payload}) {
            if (payload.data.etat=="desactivee") {
                setShowLoading(true)
                router.push('/cytoyens/attendreActivation')
            }else {
                setUser(payload.data)
                setShowLoading(false)
            }
        }).catch(function() {
            router.push('/cytoyens/login')
        }).finally(function() {
            setShowLoading(false)
        })
        V.current=false
      }
    },[])

    return <userContent.Provider value={[user,setUser]}> 
        <Modal className='d-flex align-items-center justify-content-center' show={showLoadign} setShow={setShowLoading} noHeader noFooter>
            <Circles></Circles>
        </Modal>
        {showLoadign || children} 
    </userContent.Provider>
}