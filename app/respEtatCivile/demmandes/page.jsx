"use client"
import { useEffect, useState } from 'react'
import './page.css'
import RAboutDemande from '@/_components/Resp/EtatCivile/RAboutDemande'
import { useDispatch } from 'react-redux'
import { getDemandeSansTicket } from '@/lib/features/demande/demande'

export default function ListeDemmande() {
    const [DSansTicket,setDSansTicket]=useState([]);
    const [selected,setSelected]=useState()
    const [A,setA]=useState(0)
    const dispatch=useDispatch();
    useEffect(function() {
        dispatch(getDemandeSansTicket()).then(function({payload}) {
            setDSansTicket(payload.data)
        })
    },[A])

    return  <>
        <main className="d-flex flex-column align-items-center justify-content-start __admin_page__">
            <div className="d-flex align-items-center justify-content-between flex-grow-1 col-12 __list_inscription___ ">
                <div className="d-flex flex-column shadow-lg align-items-center justify-content-start col-6 col-md-3 h-100 ___z___" >
                    <div className="col-12 __titre_liste__" style={{height:"60px"}}> Liste des demandes a approuver</div>
                    <div className="text-light d-flex flex-column align-items-center justify-content-start col-12 flex-grow-1 __cont__" style={{maxHeight:'calc(100vh - 120px - 60px)'}}>
               
                        {
                            DSansTicket.map((demande,index)=>(<div onClick={()=>{setSelected(demande)}} key={index} className="col-11 text-gray mt-2 border shadow rounded-2 __l_insc_item__">
                            <div className="m-1 __img_"></div>
                                <div className="m-1 col d-flex flex-column"> 
                                    <span> {demande.serviceDemmande} </span>
                                    <span>le {demande.dateDemmande}</span>
                                </div>
                            </div>))
                        }
                        
                        
                    </div>
                </div>

                <RAboutDemande demande={selected}></RAboutDemande>
            </div>
    </main>
    </>
}