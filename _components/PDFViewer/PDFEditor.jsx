"use client"
import React, { useRef, useEffect } from 'react';
import './PDFEditor.css';
import { CrossCircledIcon } from '@radix-ui/react-icons';

const PDFEditor = ( {titre,noHeader} ) => {
	const viewer = useRef(null);
	const value =useRef(true)
	// if using a class, equivalent of componentDidMount 
	useEffect(() => {
	if (value.current) {
		import('@pdftron/webviewer').then((module) => {
			const WebViewer=module.default
			WebViewer(
			{
				path: '/lib',
				initialDoc:"/form.pdf",
				licenseKey: 'demo:1718216527962:7fa6bcf20300000000856779a993aeb2d6a7c4a77508e4dcf30e33f86d',  // sign up to get a free trial key at https://dev.apryse.com
			},
			viewer.current,
			).then((instance) => {
			const { documentViewer, annotationManager, Annotations } = instance.Core;
			documentViewer.addEventListener('documentLoaded', () => {
				const rectangleAnnot = new Annotations.RectangleAnnotation({
				PageNumber: 1,
				// values are in page coordinates with (0, 0) in the top left
				X: 100,
				Y: 150,
				Width: 200,
				Height: 50,
				Author: annotationManager.getCurrentUser()
				});
	
				annotationManager.addAnnotation(rectangleAnnot);
				// need to draw the annotation otherwise it won't show up until the page is refreshed
				annotationManager.redrawAnnotation(rectangleAnnot);
			});
			});
		});
		
		value.current=false
	}
	}, []);

	return (
	<div className="App">
		{(!noHeader) && <div className="header"> {titre} <button className='_quitView_btn_'> <CrossCircledIcon></CrossCircledIcon> </button> </div>}
		<div className="m-0 webviewer" ref={viewer}></div>
	</div>
	);
};

export default PDFEditor;