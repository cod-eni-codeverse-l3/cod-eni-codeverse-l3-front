"use client"
import React, { useState,useCallback } from 'react';
import Image from 'next/image';
import {useDropzone} from 'react-dropzone'
import FilePreview from './FilePreview';
import './DropZone.css'


export default function DropZone({file,setFile,acceptedExt,viewHeight,textColor}) {
  const [showAlert,setShowAlert]=useState(false)

  const onDrop = useCallback(acceptedFiles => {
    const extension = acceptedFiles[0].name.split('.')[acceptedFiles[0].name.split('.').length-1];
    const formData=new FormData()
    if (acceptedExt.includes(extension) || acceptedExt.includes('*'))setFile(acceptedFiles[0])
    else{
      setShowAlert(true)
      setTimeout(() => {
        setShowAlert(false)
      }, 3000);
    }
  }, [])
  const {getRootProps, getInputProps, isDragActive} = useDropzone({onDrop})

  return <>
    {
      showAlert && (
        <div className="alert alert-danger position-absolute" role="alert">
         extension invalide
        </div>
      )
    }
    <div data-testid="_dropzone_container" className={'col-12 d-flex flex-column align-items-center border-primary rounded __drop__zone '+(textColor || " text-light ")} style={{height:(viewHeight || "120px")}} {...getRootProps()}>
      <input data-testid='_file_input' {...getInputProps()} />
      {
        isDragActive ?
          <p className='mt-2 __dropzone_txt'>Deposer le CV içi ...</p> :
          <p className='mt-2 __dropzone_txt'>Deposer le CV içi ou cliquer pour selectionner un fichier (.pdf seulement)</p>
      }
    </div>
    {file ? <FilePreview textColor={textColor} file={file}></FilePreview>:''}
  </>
}