export default function FilePreview({ file,textColor }) {
  const { name } = file;
  return (
    <div data-testid="__file_preview">
      <p className={"badge "+(textColor || 'text-light')}>{name}</p>
    </div>
  );
}
