"use client"
import { useDispatch } from "react-redux"
import "./Ticket.css"
import { DownloadIcon } from '@radix-ui/react-icons'
import { useState } from "react"
import { useEffect } from "react"
import { verifierCIN } from "@/lib/features/citoyens/citoyens"
import { createTicket } from "@/lib/features/demande/demande"
import { modifDemande } from "@/lib/features/demande/demande"

export default function Ticket({demande}) {
    const dispatch=useDispatch()
    const [citoyen,setcitoyen]=useState()

    const [A,setA]=useState(0)
    useEffect(function() {
        dispatch(verifierCIN(demande.numCINDemmande)).then(function({payload}) {
            setcitoyen(payload.data);
        })
    },[A])

    const onClicSendTicket=function() {
     
        const newTicket={
            idTicket: "",
            numCIN: citoyen.numCIN,
            sommeT: document.getElementById('somme').value,
            idDemmande: demande.idDemmande,
            lienRecep: document.getElementById('lieu').value,
            dateRecep: document.getElementById('date').value
          }

          dispatch(createTicket(newTicket)).then(function({payload}) {
            const newDemman={...demande,idTicket:payload.data.idTicket}
            return dispatch(modifDemande(newDemman))
        }).then(function() {
            setA(()=>(A+1))
        })
    }
    
    return <>
        <div className="d-flex  align-items-center justify-content-start mt-3 col-11 bg-light text-dark rounded-2 __CIN_user__">
        <div className="col h-100  flex-column d-flex align-items-center justify-content-start __ticket__"> 
            <h3 className="mt-2">CitizenConnect</h3>
            
            <div className="col-10 m-1 text-center">
                <h5> {demande.serviceDemmande} </h5>
            </div>
            <div className="col-10 m-1">
                <span className="d-flex justify-content-between"> <span>Demandé le: </span> <span>{demande.dateDemmande}</span> </span>
            </div>
            <div className="col-10 m-1">
                <span  className="d-flex justify-content-between"> <span>Nom & Prenom/CIN: </span> <span>{citoyen?.nom+ " "+citoyen?.prenom} / {citoyen?.numCIN}</span> </span>
            </div>
            <div className="col-10 m-1">
                <span className="d-flex justify-content-between"> <span>Date de recuperation:</span>  <input type="date" name="date" id="date" /></span>
            </div>
            <div className="col-10 m-1">
                <span className="d-flex justify-content-between"> <span>Lieu de recuperation:</span>  <input type="text" name="lieu" id="lieu" /></span>
            </div>
            <div className="col-10 m-1">
                <span className="d-flex justify-content-between"> <span>Somme:</span>  <input type="number" name="somme" id="somme" /></span>
            </div>
            <div className="col-10 m-1">
                <span className="d-flex justify-content-between"> <span>reference payement: </span> <span>{demande.refMvola}</span> </span>
            </div>

            <div className="__payee__">
                PAYE
            </div>
        </div>
    </div>
    <div className="mt-4 col-11">
        <button onClick={onClicSendTicket} className="btn btn-success"> Envoyer le ticket au demandeur </button>
    </div> 
    </>
}