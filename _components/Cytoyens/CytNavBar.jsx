"use client"
import './CytNavBar.css'
import { useRouter } from 'next/navigation'
import { userContent } from '@/app/cytoyens/[numCIN]/layout'
import { useContext } from 'react'
import { logOut } from '@/lib/services/userService'


export default function CytNavBar({routeName}) {
    const router = useRouter()
    const [user,setUser]=useContext(userContent)

    return (user && <nav className="shadow-lg  d-flex align-items-center justify-content-between col-12 m-0 __cyt_nav__"> 
    <h4 className='text-light text-bold'>CytizenConnnect</h4>

    <div className='text-light'>
        <div onClick={()=>{router.push('/cytoyens/'+user?.numCIN)}} className={"p-2 m-3 mt-0 mb-0 __cyt_nav_m_i__ "+(routeName=="Accueil"?"_cyt_nav_selected_":"")}>Accueil</div>
        <div onClick={()=>{router.push('/cytoyens/'+user?.numCIN+'/EtatCivil/ListeDemmande')}} className={"p-2 m-3 mt-0 mb-0 __cyt_nav_m_i__ "+(routeName=="EtatCivil"?"_cyt_nav_selected_":"")}>État civil</div>
        <div onClick={()=>{router.push('/cytoyens/'+user?.numCIN+'/identite')}} className={"p-2 m-3 mt-0 mb-0 __cyt_nav_m_i__ "+(routeName=="Identite"?"_cyt_nav_selected_":"")}>Identité</div>
        <button className='btn btn-success' onClick={()=>{router.push('/cytoyens/login');logOut()}}> Se Déconnecter </button>
    </div>
</nav> )
}