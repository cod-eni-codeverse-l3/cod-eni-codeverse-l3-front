import CytTicket from '@/_components/Ticket/CytTicket';
import './AboutDemande.css'
import { DownloadIcon } from '@radix-ui/react-icons'

export default function AboutDemande({demande}) {
    console.log(demande);
    return <>
        {
            demande?
            <div className="text-light d-flex flex-column align-items-center justify-content-start col h-100 __about__demmand__" style={{overflowY:"auto"}}>
                <div className="d-flex flex-column align-items-start justify-content-start mt-2 col-11 __about_user__">
                    <h3 style={{fontWeight:"bold"}}>Informations sur la demande:</h3>
                    <p className="m-3 mt-0 mb-2">Objet demmandeé: {demande?.serviceDemmande}</p>
                    <p className="m-3 mt-0 mb-2">Date de la demmande: {demande?.dateDemmande} </p>
                    <p className="m-3 mt-0 mb-2">Numero telephone de paiement: {demande?.numTel}</p>
                    <p className="m-3 mt-0 mb-2">Reference Mvola du Paiement:{demande?.refMvola}</p>
                    <p className="m-3 mt-0 mb-2">Imformations requises: </p>
                    <p style={{marginLeft:"60px"}}>{demande?.infoRequis}</p>
                    { (demande.listeFic!="") && <h5>Fichier requise:</h5>}
                    { (demande.listeFic!="") && <a href={"https://localhost:7223"+demande.listeFic} target="_blank" download> Cliquer pour telecharger </a>}
                    
                </div>

                <h3 style={{fontWeight:"bold"}} className="mt-3 col-11">Mon ticket:</h3>
                <div className="d-flex align-items-center justify-content-start mt-3 col-11 __CIN_user__">
                    <div className="col h-100 border d-flex align-items-center justify-content-center "> 
                        {
                            (demande.idTicket != "")? <CytTicket demande={demande} idTicket={demande.idTicket}></CytTicket> : "Le ticket"
                        }
                    </div>
                    <div className="align-items-center justify-content-center __out_download__">
                        <button className="rounded __down_ticket__"> <DownloadIcon></DownloadIcon> </button>
                    </div>
                </div>           
            </div>
            :
            <div className="text-light d-flex flex-column align-items-center justify-content-center col h-100 __about__demmand__" style={{overflowY:"auto"}}>
                veuillez selectionner un sur la liste a cotée
            </div>
        }
    </>
}