import "./AcceuilContent.css"

export default function AcceuilContent(){
    return <div className="col-12 __acceuil_content_c__" style={{minHeight:'calc(100vh - 60px)'}}>
        <div className="text-light col-12 __acceuil_content__" style={{height:"calc(100vh - 60px)"}}>
            <div className="m-5 mt-0 mb-0 col-6 pt-5 h-100">
                <h1 style={{fontSize:"80px"}} className=" mb-0 mt-5">Bienvenue sur le site</h1>
                <h1 style={{fontSize:"40px"}} className=" mb-0 ">De gestion des services d'etat civile</h1>
                <h1 style={{fontSize:"30px"}} className=" mb-0 mt-5">Explorer les services pour en savoir plus</h1>
                <a href="#__savoir_plus__"><button className="text-decoration-none mt-3 btn btn-primary">En savoir plus</button></a>
            </div>
        </div>

        <div className="col-12 bg-light __savoir_plus__" id="__savoir_plus__">
            <div className="d-flex align-items-center justify-content-evenly m-4 __a_propos__">
                <div className="m-2 col-6">
                    <span className="d-block mb-3" style={{fontWeight:"bolder",fontSize:"30px"}}>A propos</span>
                    <span className="d-block mb-3" style={{fontWeight:"bold",fontSize:"18px"}}>Simplifiez vos démarches administratives avec une gamme complète de services en ligne, incluant la délivrance d'actes d’état civil ainsi que d’autres. Cette plateforme est conçue pour rendre vos interactions avec l'administration rapides et efficaces tout en étant sécurisé.</span>
                </div>

                <div className="col-4 h-100 bg-dark"></div>
            </div>

            <div className="d-flex align-items-center justify-content-evenly m-4 __a_propos__">
                <div className="col-4 h-100 bg-dark"></div>

                <div className="m-2 col-6">
                    <span className="d-block mb-3" style={{fontWeight:"bolder",fontSize:"30px"}}>Services proposées</span>
                    <span className="d-block mb-3" style={{fontWeight:"bold",fontSize:"18px"}}>Les services qui vous seront proposés sont diverses, comme la demande d’acte de naissance, la demande d’acte de décès, la légalisation de photocopie de CIN, et le changement d’adresse pour la section Etat civil. Comme service fiscal, le paiement d’impôt vous est proposé.</span>
                </div>           

            </div>
        </div>
    </div>
}