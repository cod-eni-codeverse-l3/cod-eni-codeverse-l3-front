/** @type {import('next').NextConfig} */
const nextConfig = {
    images:{
        remotePatterns: [
            {
              protocol: 'https',
              hostname: 'localhost',
              port: '7223',
              pathname: '/Upload/**',
            },
          ],
    }
};

export default nextConfig;
