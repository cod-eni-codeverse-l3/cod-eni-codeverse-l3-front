import citoyensReducer from './features/citoyens/citoyens'
import demandeReducer from './features/demande/demande'
import { combineReducers, configureStore } from '@reduxjs/toolkit'

const rootReducer=combineReducers({
    citoyens:citoyensReducer,
    demande:demandeReducer
})

export const makeStore=()=>configureStore({
    reducer:rootReducer,
    middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        // Ignore these action types
        ignoredActions: ['dem/modif/fulfilled','ticket/create/fulfilled',"demande/getDemandeSansTicket/fulfilled",'user/verif/fulfilled',"user/signup/fulfilled","file/upload/fulfilled",'user/login/fulfilled',"demande/create/fulfilled","demande/getByNumCin/fulfilled","user/getUserDesactiver/fulfilled","user/modif/fulfilled"],
        // Ignore these field paths in all actions
        ignoredActionPaths: ['meta.arg', 'payload.timestamp'],
        // Ignore these paths in the state
        ignoredPaths: ['items.dates'],
      },
    }),
})