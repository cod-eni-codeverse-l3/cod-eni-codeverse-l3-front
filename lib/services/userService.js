export function senDuser({numCIN,mdp}) {
    localStorage.setItem('numCIN',numCIN)    
    localStorage.setItem('mdp',mdp)
}

export function getAdmin() {
    const nomUser = localStorage.getItem("nomUser")
    const hmdp = localStorage.getItem('mdp')
    return {nomUser,hmdp}
}

export function isLogged() {
    const numCIN = localStorage.getItem("numCIN")
    const mdp = localStorage.getItem('mdp')
    if (numCIN && mdp) {
        return {numCIN,mdp}
    }
    return false
}

export const logOut=function() {
    localStorage.clear("numCIN")
    localStorage.clear("mdp")
}