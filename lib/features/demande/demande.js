import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
axios.defaults.baseURL=process.env.NEXT_PUBLIC_API_HOST

/**
 * 
 * @param {Array} liste 
 */
function getMax(liste){
    let max=-1
    liste.forEach(function(element) {
        if (element>max) {
            max=element
        }
    })
    return max
}

const getNextId=async function() {
    let NextID=""
    const {data}=await axios.get("/api/demmandes")
    if (data.length === 0 ) NextID="D1"
    else{
        let liste=data.map(element => (parseInt(element.idDemmande.slice(1))))
        NextID="D"+( getMax(liste) +1)
    }
    return NextID
}

const getNextIdT=async function() {
    let NextID=""
    const {data}=await axios.get("/api/tickets")
    if (data.length === 0 ) NextID="T1"
    else{
        let liste=data.map(element => (parseInt(element.idTicket.slice(1))))
        NextID="T"+( getMax(liste) +1)
    }
    return NextID
}

export const createDemmande=createAsyncThunk("demande/create",async function(arg,thunkAPI) {
    const idDemmande= await getNextId()
    let tmp = {idDemmande,...arg}
    console.log(tmp);
    return axios.post('/api/demmandes',tmp)
})

export const createTicket=createAsyncThunk("ticket/create",async function(arg,thunkAPI) {
    const idDemmande= await getNextIdT()
    let tmp = {...arg,idTicket:idDemmande}
    console.log(tmp);
    return axios.post('/api/tickets',tmp)
})

export const getListeDemmandeByNumCin=createAsyncThunk('demande/getByNumCin',async function (numCIN,thunkAPI) {
    return axios.get(`/getnumCIN/${numCIN}`)
})

export const getDemandeSansTicket=createAsyncThunk('demande/getDemandeSansTicket',async function (numCIN,thunkAPI) {
    return axios.get(`/getDemandeSansTicket`)
})

export const modifDemande=createAsyncThunk("dem/modif",async function(newDemmand,thunkAPI) {
    return axios.put(`/api/demmandes/${newDemmand.idDemmande}`,newDemmand);
})


const demandeSlice=createSlice({
    name:"demande",
    initialState:[
    ],
    reducers:{
        
    },
    extraReducers:(builder)=>{
       
     }
})

export const { } =demandeSlice.actions

export default demandeSlice.reducer