import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
axios.defaults.baseURL=process.env.NEXT_PUBLIC_API_HOST

export const createCytoyent=createAsyncThunk("user/signup",async function(arg,thunkAPI) {
    return axios.post('/api/utilisateurs',{
        numCIN: arg.numCIN,
        role: "citoyen",
        adresse: arg.Adresse,
        nom: arg.nom,
        prenom: arg.prenom,
        sexe: arg.Sexe,
        datenaiss: arg.dateNaiss,
        proffession: arg.Proffession,
        photo: "https://localhost:7223"+arg.pdp,
        ciNrecto: "https://localhost:7223"+arg.CINRect,
        ciNverso: "https://localhost:7223"+arg.CINVerso,
        etat: "desactivee",
        mdp: arg.mdp
      })
})

export const modifCytoyent=createAsyncThunk("user/modif",async function(newUser,thunkAPI) {
    console.log({
        ...newUser,
        etat: "activee",
      });
    return axios.put(`/api/utilisateurs/${newUser.numCIN}`,{
        ...newUser,
        etat: "activee",
      })
})

export const verifierCIN=createAsyncThunk("user/verif",async function(numCIN,thunkAPI) {
    return axios.get(`/api/utilisateurs/${numCIN}`);
})

export const deleteUser=createAsyncThunk("user/delete",async function(numCIN,thunkAPI) {
    return axios.delete('/api/utilisateurs/'+numCIN);
})

export const getTicketByDem=createAsyncThunk("ticket/get",async function(idDemmande,thunkAPI) {
    const {data} = await axios.get(`/getTicket/${idDemmand}`);
    console.log(data);
})

export const getUserDesactiver=createAsyncThunk("user/getUserDesactiver",async function(numCIN,thunkAPI) {
    return axios.get('/getUserDesactiver');
})

export const login=createAsyncThunk("user/login",async function({numCIN,mdp},thunkAPI) {
    return axios.get(`/api/utilisateurs/login/${numCIN}/${mdp}`);
})



export const uplodFile=createAsyncThunk("file/upload",async function(file,thunkAPI) {
   return axios.post('/UploadFile',file,{
        headers:{"Content-Type":"form-data"}
   })
})

const citoyenSlice=createSlice({
    name:"citoyen",
    initialState:[  
    ],
    reducers:{
        
    },
    extraReducers:(builder)=>{
        builder.addCase(createCytoyent.rejected,function (state,action) {
            console.log("error");
        }).addCase(createCytoyent.fulfilled,function (state,action) {
            console.log(action);
        })

        builder.addCase(uplodFile.rejected,function (state,action) {
            console.log("error");
        })
     }
})

export const { } =citoyenSlice.actions

export default citoyenSlice.reducer